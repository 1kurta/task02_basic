package com.epam2;

public class Number {
    public static void main(String[] args)
    {

        int maxNumber = 100, previousNumber = 0, nextNumber = 1;
        System.out.println("Fibonacci Series of "+maxNumber+" numbers:");

        int i=1;
        while(i <= maxNumber)
        {
            System.out.println(previousNumber +" ");
            int sum = previousNumber + nextNumber;
            previousNumber = nextNumber;
            nextNumber = sum;
            i++;
        }

    }

}

